# frozen_string_literal: true

class AppEnvironment
  def self.production?
    ENV['APP_ENVIRONMENT'].to_sym == :production
  end

  def self.development?
    ENV['APP_ENVIRONMENT'].to_sym == :development
  end

  def self.test?
    ENV['APP_ENVIRONMENT'].to_sym == :test
  end
end
