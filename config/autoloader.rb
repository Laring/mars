# frozen_string_literal: true

require 'i18n'
require 'pry-rails'
require 'singleton'
require 'terminal-table'

# ENV['APP_ENVIRONMENT'] ||= 'production'
ENV['APP_ENVIRONMENT'] ||= 'development'

require_relative '../app/modules/exceptions'
require_relative '../app/modules/validator'

require_relative 'app_environment'

require_relative '../app/services/mars_service'
require_relative '../app/entities/validatable_entity'
require_relative '../app/entities/plateau'
require_relative '../app/entities/rover'
require_relative '../app/states/welcome_state'
require_relative '../app/states/move_rovers_state'
require_relative '../app/states/finish_state'

I18n.load_path << Dir["#{File.expand_path('config/locales')}/*.yml"]
