# frozen_string_literal: true

RSpec.describe FinishState do
  let(:finish_msg) do
    <<~PUBLISHED
      #{I18n.t(:finish_msg)}
    PUBLISHED
  end

  describe '#call' do
    it 'prints finish message' do
      expect do
        rovers = [
          { x: 1, y: 2, d: 'N', i: 'LMLMLMLMM' }, # 1 3 N
          { x: 3, y: 3, d: 'E', i: 'MMRMMRMRRM' } # 5 1 E
        ]
        expect { described_class.call(rovers) }.to output(finish_msg).to_stdout
      end.to raise_error(SystemExit)
    end
  end
end
