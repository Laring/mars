# frozen_string_literal: true

module Validator
  def positive_integer?(value)
    value.is_a?(Integer) && value >= 0
  end
end
