# frozen_string_literal: true

module Exceptions
  class EntityValidateError < StandardError
    def initialize(msg = 'Cannot create object')
      super
    end
  end
end
