# frozen_string_literal: true

class Plateau < ValidatableEntity
  attr_accessor :top, :right
  attr_reader :busy_fields

  def initialize(params)
    super(params)
    create
    @top = @params[:top]
    @right = @params[:right]
  end

  def less_then_step(coordinate, np_val)
    rover_to_plateau = { x: :right, y: :top }
    instance_variable_get("@#{rover_to_plateau[coordinate]}") < np_val
  end

  def deploy_rovers(rovers)
    rovers.map { |rover| Rover.new(rover) }
  end

  def busy_fields=(rovers)
    @busy_fields = rovers.map { |r| "#{r.x}:#{r.y}" }
  end

  def update_busy_fields(new_coordinates, old_coordinates)
    @busy_fields << new_coordinates
    @busy_fields.delete(old_coordinates)
  end

  private

  def validate
    @errors << I18n.t('errors.plateau_top_right_exists_msg', coordinate: "'top'") if @params[:top].nil?
    @errors << I18n.t('errors.plateau_top_right_exists_msg', coordinate: "'right'") if @params[:right].nil?
    @errors << I18n.t('errors.positive_integer_msg', coordinate: "'top'") unless positive_integer?(@params[:top])
    @errors << I18n.t('errors.positive_integer_msg', coordinate: "'right'") unless positive_integer?(@params[:right])
    @errors << I18n.t('errors.plateau_less_msg', coordinate: "'top'") unless @params[:top] > 1
    @errors << I18n.t('errors.positive_integer_msg', coordinate: "'right'") unless @params[:right] > 1
  end
end
