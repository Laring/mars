# frozen_string_literal: true

class Rover < ValidatableEntity
  attr_accessor :x, :y, :d, :i

  XY_MAP = {
    W: { x: '-' },
    E: { x: '+' },
    N: { y: '+' },
    S: { y: '-' }
  }.freeze

  def initialize(rover)
    super(rover)
    create
    # check_params(rover)
    @x = @params[:x]
    @y = @params[:y]
    @d = @params[:d]
    @i = @params[:i]
  end

  def xy_map
    XY_MAP
  end

  def set_step_direction(instruction)
    rose = %w[N E S W]
    if instruction === 'R'
      @d = rose[rose.index(d) + 1].nil? ? rose[0] : rose[rose.index(d) + 1]
    elsif instruction === 'L'
      @d = rose[rose.index(d) - 1]
    end
  end

  def get_old_position_value(coordinate)
    instance_variable_get("@#{coordinate}")
  end

  def get_coordinates(coordinate, position)
    coordinate === :x ? "#{position}:#{@y}" : "#{@x}:#{position}"
  end

  def set_position(coordinate, new_position_value)
    instance_variable_set("@#{coordinate}", new_position_value)
  end

  private

  def validate
    @errors << I18n.t('errors.no_x_msg', coordinate: 'x') if @params[:x].nil?
    @errors << I18n.t('errors.no_y_msg', coordinate: 'y') if @params[:y].nil?
    unless positive_integer?(@params[:x]) || (@params[:x]).zero?
      @errors << I18n.t('errors.positive_integer_msg',
                        coordinate: 'x')
    end
    unless positive_integer?(@params[:y]) || (@params[:y]).zero?
      @errors << I18n.t('errors.positive_integer_msg',
                        coordinate: 'y')
    end

    @errors << I18n.t('errors.direction_exists_msg') if @params[:d].nil?

    direction = @params[:d].scan(/N|E|S|W/).map(&:to_s).join
    @errors << I18n.t('errors.direction_empty_msg') if direction.empty?
    @params[:d] = direction

    @errors << I18n.t('errors.rover_i_exists_msg') if @params[:i].nil?
    instructions = @params[:i].scan(/M|L|R/).map(&:to_s).join
    @errors << I18n.t('errors.rover_i_empty_msg') if instructions.empty?
    @params[:i] = instructions
  end
end
