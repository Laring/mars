# frozen_string_literal: true

class ValidatableEntity
  include Validator

  def initialize(params)
    @params = params
    @errors = []
  end

  def create
    raise Exceptions::EntityValidateError, @errors unless valid?
  end

  protected

  def valid?
    validate
    @errors.empty?
  end

  def validate
    raise NotImplementedError
  end
end
