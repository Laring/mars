# frozen_string_literal: true

class MoveRoversState
  include Singleton
  attr_reader :plateau, :rovers

  class << self
    def call(plateau, rovers)
      new(plateau, rovers).call
    end
  end

  def initialize(plateau, rovers)
    @plateau = plateau
    @rovers = rovers
  end

  def call
    move_rovers
  end

  private

  def move_rovers
    @rovers.each do |rover|
      move_rover(rover)
    end
  end

  def move_rover(rover)
    rover.i.each_char.with_index(1) do |i, index|
      follow_instruction(rover, i, index)
    end
  end

  def follow_instruction(rover, i, step_index)
    rover.set_step_direction(i)

    instruction = rover.xy_map[rover.d.to_sym]
    coordinate = instruction.keys[0]
    op_val = rover.get_old_position_value(coordinate)

    if i === 'M'
      np_val = op_val.send(instruction[coordinate], 1)
      new_coordinates = rover.get_coordinates(coordinate, np_val)
      check_busy_fields(new_coordinates, coordinate, np_val, step_index)
      rover.set_position(coordinate, np_val)
      @plateau.update_busy_fields(new_coordinates, rover.get_coordinates(coordinate, op_val))
    end
  end

  def check_busy_fields(new_coordinates, coordinate, np_val, step_index)
    if @plateau.busy_fields.include?(new_coordinates) || @plateau.less_then_step(coordinate, np_val)
      raise StandardError,
            "Rover stopped on step #{step_index}. Next rover position is busy. Next expected position: #{coordinate}: #{np_val}. Plateau sise: x:#{@plateau.right}, y:#{@plateau.top}."
    end
  end
end
