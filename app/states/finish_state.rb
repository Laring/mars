# frozen_string_literal: true

class FinishState
  include Singleton
  attr_reader :rovers

  class << self
    def call(rovers)
      new(rovers).call
    end
  end

  def initialize(rovers)
    @rovers = rovers
  end

  def call
    finish_message
    exit
  end

  private

  def finish_message
    puts I18n.t(:finish_msg)
    table = Terminal::Table.new headings: %w[x y direction], rows: @rovers.map { |r| [r.x, r.y, r.d] }
    puts table
  end
end
