# frozen_string_literal: true

class MarsService
  include Singleton

  class << self
    def call
      new.call
    end
  end

  def call
    plateau_size_message
    plateau = Plateau.new(plateau_top_right)
    rovers = plateau.deploy_rovers(rovers_deployment)
    plateau.busy_fields = rovers

    MoveRoversState.call(plateau, rovers)

    FinishState.call(rovers)
  rescue Exceptions::EntityValidateError || Exception => e
    print e
    call
  end

  private

  def plateau_size_message
    print I18n.t(:plateau_size_msg)
  end

  def plateau_top_right
    print I18n.t(:plateau_right_msg)
    right = gets.chomp.to_i
    print I18n.t(:plateau_top_msg)
    top = gets.chomp.to_i
    { top: top, right: right }
  end

  def rovers_deployment
    puts I18n.t(:rovers_cnt_msg)
    cnt = gets.chomp.to_i
    unless cnt.positive?
      raise Exceptions::EntityValidateError,
            I18n.t('errors.positive_integer_msg', coordinate: "'cnt'")
    end

    rovers = []
    cnt.times do |i|
      rover_num = i + 1
      puts I18n.t(:rover_coordinate_msg, rover_num: rover_num, coordinate: 'x')
      x = gets.chomp.to_i
      puts I18n.t(:rover_coordinate_msg, rover_num: rover_num, coordinate: 'y')
      y = gets.chomp.to_i
      puts I18n.t(:rover_direction_msg, rover_num: rover_num)
      d = gets.chomp.upcase
      puts I18n.t(:rover_instruction_msg, rover_num: rover_num)
      i = gets.chomp.upcase
      rovers.push({ x: x, y: y, d: d, i: i })
    end

    rovers
  end
end
